SHANTANU SIKDAR						sikdar.shantanu@live.com / 
9423577420, 9130532164						sikdar.shantanu@gmail.com
http://www.linkedin.com/in/shantanu-sikdar

Experience Summary:
	160+ months into S/W development using Java, Spring, Javascript. Hands on SQL, PL/SQL on MySQL.
	Involved in requirement analysis/gathering, designing, technology migration, feasibility study, POCs.
	Versed with DS/Algo, Design-Patterns. Reduce existing LOCs, used SOLID principles, part of rapid action environment fixes. 
	Played with various APIs like Lucene, Dynamic-Jasper, EWS, 
	Worked on tools like VSS, SVN, Harvest, Hudson
	Well to do communication, analytical, interpersonal and presentation skills.
	Project planning, task breakout, change management setting up UAT and Production Servers, Client visits for Presales, requirement gathering,

Skills Sets:
Java Technologies	Java, J2EE, Struts(1.x, 2.2), Spring (Core, MVC, JMS, Batch) 3.1
Web Technologies	Java Script(Prototype.js, JQuery, dhtmlxgrid.js), Ajax, CSS
Web Server	Apache Tomcat(5.X,7.0). JBoss 7, WebSphere 7
Various Dev/Test/Control Tools	Eclipse, Netbeans, Jira, Bugzilla, SQL Yog, Hudson, SVN, VSS, Harvest. IBM MQ Series. GIT
Database Technologies	 MySQL 5.0, MS SQL Server 2008, Oracle.
Other APIs	EWS, Dynamic Jasper, Quartz, SVNKit, WebServices (REST and SOAP). Jackson
Other responsibilities	Project planning, task breakouts, sprint planning/reviews

Education:
•	Post Graduation Diploma in Advanced Computing (CDAC) (C-DAC ACTS, Pune. February-2006 to July-2006).
•	Bachelor of Engineering (National Institute of Technology Durgapur, West Bengal. July 2000 to July 2004.)

Working Experience: 
At Organization: Barclays Technology Center							 (June 2017 – till Date)
Money Mentors
Any individual wanted to manage their money, can use this service. In which they can get in touch with Barclays appointed personal by means of video, audio or face2face.
		Roles & Responsibilities: 
•	Initial setup, coding the various controller and service layer interfaces. Helping the team with various helper classes for data manipulation and formatting.
•	Managed a team of 2 UI developer, 2 testers, 1 Business Development and 2 Developers. Assigning task relevant to the application and their role.
•	Interaction with various teams for their services exposed.
•	Reporting the progress to the business.

Technology Stack 
JRE 1.6, spring3.0, REST, Jackson
From the postcode, by calling google’s geolocation service, nearest Barclays branches are fetched. Further using the branch-id, we fetched the mentors from the various service exposed by bank.	
	

OneTouch Contact
Bringing all the contact details of the personal and business customer of the Barclays banks under one roof. Presently they are scattered in various application.	Roles & Responsibilities: 
•	Understand the API exposed by various other applications having the customer contact details, bringing them to same line and showing the customer their relevant data.
•	Interaction with various other team to get the exposed APIs understanding, request them to do changes if needed, then consuming the API.
•	Managed a team of 2 UI developer, 2 testers and 2 Business Development folks. Assigning task relevant to the application and their role.


Technology Stack
JRE 1.6, spring3.0, REST, javascript, EJS (templating).	
Using onion architecture, where one layer is UI the other layers are backed layers which talks with various exposed APIs.	

SIFO (Security Infrastructure )
As the need of the hour, fixing vulnerabilities and secure  coding has taken priority. Thus, this projects comes into picture and tries to eradicate coding loop holes.    		Roles & Responsibilities: 
•	Implemented and suggested various coding fixes for various java vulnerabilities, suggested by tools checkmarx and sonarqube across all the java components of channel application. 
•	From individual contributor to grown up to a team of 4 resources who are doing the same task I’m doing. 

Technology Stack
JRE 1.6.	
	


At Organization: Sears Holdings  							 (March 2016 – Apr 2017)
C/I MA
Cost/Item modeling application, used by sears and kmart shop vendors. Who wanted to sell their items through sears portal or  through sears/kmart physical shop.		Roles & Responsibilities: 
•	Understand the business requirements and the existing application and decide on design changes for the new requirement in the application. 
•	Managing a team and assigning task relevant to the application, kind of sprint owner role.
•	Migration to spring 4.2.x.
Technology Stack
JRE 1.8, spring3.0, Struts1.2, oracle, jQuery.	
	

At Organization: Inautix Technologies (A BNY Mellon Company)		 (September 2012 – March 2016)
OnBoarding Portal
Portal to migrate several BNYM report generating portals (RGPs) under one enterprise-portal (ERS).  The development team of the various RGPs use this portal to migrate to ERS. 	Roles & Responsibilities: (20 months)
•	Product owner, sprint planner, standups, task breakouts etc basically followed agile.
•	Modeled the architecture of the application from verbal feedback from individual RGP team. 
•	Deciding the technology from the limited BNYM stack of technology.
•	There is no database support for data persistence, following up a lot with the management but failed. Thus, used SVN for data persistence. It involved a lot of file IO. 
•	Modeled with Composite design patterns
•	Handled business people, who don’t understand anything about technology. 
Technology Stack
JRE 1.7, Spring (MVC,Core), javascript, SVNKit (for data persistence)	
Technology Summary
Multipage UI application, where the UI components are defined and designed in spring-context xml.  The portal creates report pertaining *.sql, *.csv files for every report to migrate. Then these files are pushed to SVN using svnkit for persistence.	

AppCentral
This Idea I submit as a part of hackathon event. The idea is to centralized all the application in a one page app. Various means I used are webservices, scraping 		Roles & Responsibilities: (6 months)
•	Hackathon is an event across BNY Mellon. I
took part in it as an individual and submit the idea, of integrating various user’s application at one page. 
•	Each and every thing of the application starting from idea conceptualizing, designing the architecture, coding, presenting and start implementing.
•	Coding the algorithms for data scrapping from the pages which do not provide any APIs.
•	Uploading/checking out file to/from repository.
Technology Stack
JDK 1.6, Spring (MVC,Core), Jersey(REST), EWS, JQuery
JSoup(for scrapping)	
Technology Summary
Spring MVC helped with the controller classes to interact with various POJOs. Using these POJOs I use EWS to integrate MS Exchange, Jersey to integrate Jira, Jsoup for scrapping, SVNKit for SVN integration etc.	

Workbench Corporate Action 		Roles & Responsibilities: (8 months)
•	Remodeling the architecture design change by using spring DI and take out the db queries from java files and put them into context xml files. Now the architecture is agile and open ended. Any new client comes with their set of data, we just have to add specific queries. 
•	Also get rid of row-mapper classes, instead used JPA and coding logic, got rid of many LOCs.
Also, did a presentation of the model, at the “Technology Trends" of the organization.

Technology Stack
JDK 1.6, Spring Core.
	
Technology Summary
Remodeling the existing architecture. Introduced the spring nature to the application, thus make it more easily manageable and adding a new friendly.	

Fair Value Hierarchy 	Roles & Responsibilities: 
•	Identifying probable replacement of existing technology like servlet-jsp to spring-mvc, ejb to openEjb, websphere to tomee+ etc.
•	Writing POCs using spring MDPs instead of MDBs, deploy on tomcat, configuring JNDI using tomcat etc.
•	Remodeled the whole code base into the identified technology.
Technology Stack
JDK 1.6, Dynami Jasper 5.0, quartz schedulers, Spring (MVC, JMS, Integration), JQuery 
	
Technology Summary
Existing architecture is remodeled using Spring mvc, MDBs
are replaced by MDPs, schedulers are rewritten using 
quartz, jQuery replaces the flash component. Dynamic
Jasper for generation of pdfs, xls and csv.	

At Organization: Nitman Software Pvt. Ltd. (part of Talentica Software)		 (March-2008 – September 2012)
(We are the only Indian company to be named by Gartner in Market Guide for e-Recruitment Solutions 2011)
TALENTPOOL	
Product for Corporate, with various recruitment modules, like Requisition Approvals, Positions and hiring steps, Candidate repository with their interaction history and so on	Roles & Responsibilities: (with 12 people)
•	Understand the recruitment lifecycle, product requirements with the help of techno-functional analyst. Enable the product from simple data store to process based.
•	Implemented the logic for licensing and segregation of the product modules.
•	Enable the product with quartz schedulers, REST (jersey) web-services for integration.
•	Wrote the installer using NSIS, created Portals using SOAP(axis).
•	Pre-sales visits, understanding the requirement then writing specifications.
•	Customizations and Product Enhancements.
Technology Stack
Tomcat 5.5.28, 7.0.22, JDK 1.6 and 1.7, Apache Struts1 and Struts2, MySQL 5.0.18, Velocity, TinyMCE, Quartz	

Technology Summary
Simple struts MVC framework. View manages the layout and rendering of the content with the help of JSPs, prototype.js, dhtmlxgrid.j,s tlds . The Controller layer encapsulates the behavior and is modeled using the struts action classes. The Model contains the domain logic achieved using POJO classes. 	

TALENTSCOUT	
Complete business management system for placement consultants (candidates and clients, their process and communication history, searches). It takes care of client relationships (billing, PR etc.) and candidate relationships (Interactions, PR, reports) on a single platform.	Roles & Responsibilities: (with 7 people)
•	End to end development from the product inception, created a tiered architecture, created data model, identifying various entity types, attributes and relationships. 
•	Modeled the Business tier POJOs & POJIs.
•	Supported Presentation Tier with css classes, dhtmlxgrid.js, prototype.js for document objects, tinymce etc. 
•	Developed plug-ins to integrate data from MS Outlook, Job-portals and for formatting the Resume.
•	Deploying the product at clients place. Made Utilities for the data import.
•	Helps in forming support team and then impart them the product knowledge.
•	Customizations and Product Enhancements.
Technology Stack
Tomcat 5.0.30, JDK 1.6, Struts 1.x, MySQL 5.0.18, Lucene 
 2.9. Quartz1.6.0,  ANT, NSIS	

Technology Summary
Follows the struts MVC framework. Mostly follows the Command design patterns for responding various requests.
The code base is taken from Talentpool with difference in working of the product.
	

VSell 
VSell involves organizing, automating and synchronizing
sales activities on a single platform. The overall goals are to
find, attract, and win new clients, nurture and retain the 
existing clients.	Roles & Responsibilities: (Individual Contributor)
•	Collected input from the sales team, compare other available products.
•	Setting up the development environment, data modeling, modeled various tiers.
•	Wrote the initial code base.
•	Setting up a team, done the knowledge transfer and handover the project.
Technology Stack
Tomcat 5.0.30, JDK 1.6, Struts 1.x, MySQL 5.0.18, Lucene 
2.9. Quartz1.6.0,  ANT, NSIS
	

At Organization: Ascent Informatics Pvt. Ltd. 					(November 2006 to March 2008)
FoodLogiq 	 Client: Smart Online
Data capturing of On-Farm and Processing-Plants’ food-safety programs. Uploading, managing of Lab-Tests, Certifications, Audits, Quality-management documentation. Capture supplier specific requirements with the easy to use Audit Form Builder. Buyers can track supplier compliance and certifications.	Roles & Responsibilities (6 months with 9 people):
•	Implemented using Factory Design Patterns for various business logic and presentation layer of the modules of Premise Owner and Laboratory Owner. 
•	The payment gate-way where Premise Owner pay the amount.
•	Template Method design patterns are also used to respond various request calls.
Technology Stack
Struts 1.2, Java 1.5, MySQL 5.0, Maven 1.0.2, MySQL 4.1	

One Truck	 Client:  Kewill
Interface for shipment delivery system that enables the user to enter the shipping data. It supports different carriers, having services.	Roles & Responsibilities (5months with 10 people)
•	Modeled the custom webpage, where various services can be chosen on different kind of product.
•	Used XML-RPC to submit the order and fetch back the pricing of the services chosen.
Technology Stack
PHP(5.0), Java(1.6), MySql(4.1)	

Express Manager	 Client: Unishippers
User entered shipment data processing and a relevant rate and service estimate is shown back to the user. Depending on the estimates user can confirm the shipment.	Roles & Responsibilities(5 months with 6 people)
•	Designed the order form and subsequent submission pages, shipment status.
•	Wrote a XML based RPC to submit the order data and fetch the shipment status data.
Technology Stack
JSP-Servlets, Java (1.4), Maven 1.0, MySql(4.1).	

Web Refernces:
https://dzone.com/users/784095/shantanusikdar.html?sort=articles
           	https://github.com/radnahs

Miscellaneous:
Father’s Name	: Ranjit Kumar Sikdar
Mother’s Name	: Priti Sikdar
Address 	: Flat No. k/6, Ganesh Residency, Survey No. 21/4, Dapodi, Pune-411012
Date of Birth	: 14 September 1981.
Passport No	: L7217070

 