5. Code structure
The first thing we’ll study is the building blocks of code.
Statements
Statements are syntax constructs and commands that perform actions.
We’ve already seen a statement, alert('Hello, world!'), which shows the message “Hello, world!”.
We can have as many statements in our code as we want. Statements can be separated with a semicolon.
For example, here we split “Hello World” into two alerts:
alert('Hello'); alert('World');
Usually, statements are written on separate lines to make the code more readable:
alert('Hello');
alert('World');
Semicolons
A semicolon may be omitted in most cases when a line break exists.
This would also work:
alert('Hello')
alert('World')
Here, JavaScript interprets the line break as an “implicit” semicolon. This is called an automatic semicolon insertion.
In most cases, a newline implies a semicolon. But “in most cases” does not mean “always”!
There are cases when a newline does not mean a semicolon. For example:
alert(3 +
1
+ 2);
The code outputs 6 because JavaScript does not insert semicolons here. It is intuitively obvious that if the line ends with a plus "+", then it is an “incomplete expression”, so the semicolon is not required. And in this case that works as intended.
But there are situations where JavaScript “fails” to assume a semicolon where it is really needed.
Errors which occur in such cases are quite hard to find and fix.
An example of an error
If you’re curious to see a concrete example of such an error, check this code out:
[1, 2].forEach(alert)
No need to think about the meaning of the brackets [] and forEach yet. 
We’ll study them later. For now, just remember the result of the code: 
it shows 1 then 2.
Now, let’s add an alert before the code and not finish it with a semicolon:
alert("There will be an error")

[1, 2].forEach(alert)
Now if we run the code, only the first alert is shown and then we have an error!
But everything is fine again if we add a semicolon after alert:
alert("All fine now");

[1, 2].forEach(alert)
Now we have the “All fine now” message followed by 1 and 2.
The error in the no-semicolon variant occurs because JavaScript does not assume a semicolon before square brackets [...].
So, because the semicolon is not auto-inserted, the code in the first example is treated as a single statement. Here’s how the engine sees it:
alert("There will be an error")[1, 2].forEach(alert)
But it should be two separate statements, not one. Such a merging in this case is just wrong, hence the error. This can happen in other situations.
We recommend putting semicolons between statements even if they are separated by newlines. This rule is widely adopted by the community. Let’s note once again – it is possible to leave out semicolons most of the time. But it’s safer – especially for a beginner – to use them.
Comments
As time goes on, programs become more and more complex. It becomes necessary to add comments which describe what the code does and why.
Comments can be put into any place of a script. They don’t affect its execution because the engine simply ignores them.
One-line comments start with two forward slash characters //.
The rest of the line is a comment. It may occupy a full line of its own or follow a statement.
Like here:
// This comment occupies a line of its own
alert('Hello');

alert('World'); // This comment follows the statement
Multiline comments start with a forward slash and an asterisk /* and end with an asterisk and a forward slash */.
Like this:
/* An example with two messages.
This is a multiline comment.
*/
alert('Hello');
alert('World');
The content of comments is ignored, so if we put code inside /* … */, it won’t execute.
Sometimes it can be handy to temporarily disable a part of code:
/* Commenting out the code
alert('Hello');
*/
alert('World');
Use hotkeys!
In most editors, a line of code can be commented out by pressing the Ctrl+/ hotkey for a single-line comment and something like Ctrl+Shift+/ – for multiline comments (select a piece of code and press the hotkey). For Mac, try Cmd instead of Ctrl.
Nested comments are not supported!
There may not be /*...*/ inside another /*...*/.
Such code will die with an error:
/*
  /* nested comment ?!? */
*/
alert( 'World' );
Please, don’t hesitate to comment your code.
Comments increase the overall code footprint, but that’s not a problem at all. There are many tools which minify code before publishing to a production server. They remove comments, so they don’t appear in the working scripts. Therefore, comments do not have negative effects on production at all.
Later in the tutorial there will be a chapter Code quality that also explains how to write better comments.





















6. The modern mode, "use strict"
For a long time, JavaScript evolved without compatibility issues. New features were added to the language while old functionality didn’t change.
That had the benefit of never breaking existing code. But the downside was that any mistake or an imperfect decision made by JavaScript’s creators got stuck in the language forever.
This was the case until 2009 when ECMAScript 5 (ES5) appeared. It added new features to the language and modified some of the existing ones. To keep the old code working, most modifications are off by default. You need to explicitly enable them with a special directive: "use strict".
“use strict”
The directive looks like a string: "use strict" or 'use strict'. When it is located at the top of a script, the whole script works the “modern” way.
For example:
"use strict";

// this code works the modern way
...
We will learn functions (a way to group commands) soon.
Looking ahead, let’s just note that "use strict" can be put at the start of most kinds of functions instead of the whole script. Doing that enables strict mode in that function only. But usually, people use it for the whole script.
Ensure that “use strict” is at the top
Please make sure that "use strict" is at the top of your scripts, otherwise strict mode may not be enabled.
Strict mode isn’t enabled here:
alert("some code");
// "use strict" below is ignored--it must be at the top

"use strict";

// strict mode is not activated
Only comments may appear above "use strict".
There’s no way to cancel use strict
There is no directive like "no use strict" that reverts the engine to old behavior.
Once we enter strict mode, there’s no return.
Browser console
For the future, when you use a browser console to test features, please note that it doesn’t use strict by default.
Sometimes, when use strict makes a difference, you’ll get incorrect results.
Even if we press Shift+Enter to input multiple lines, and put use strict on top, it doesn’t work. That’s because of how the console executes the code internally.
The reliable way to ensure use strict would be to input the code into console like this:
(function() {
  'use strict';

  // ...your code...
})()
Always “use strict”
We have yet to cover the differences between strict mode and the “default” mode.
In the next chapters, as we learn language features, we’ll note the differences between the strict and default modes. Luckily, there aren’t many and they actually make our lives better.
For now, it’s enough to know about it in general:
1.	The "use strict" directive switches the engine to the “modern” mode, changing the behavior of some built-in features. We’ll see the details later in the tutorial.
2.	Strict mode is enabled by placing "use strict" at the top of a script or function. Several language features, like “classes” and “modules”, enable strict mode automatically.
3.	Strict mode is supported by all modern browsers.
4.	We recommended always starting scripts with "use strict". All examples in this tutorial assume strict mode unless (very rarely) specified otherwise.


























7. Variables
Most of the time, a JavaScript application needs to work with information. Here are two examples:
1.	An online shop – the information might include goods being sold and a shopping cart.
2.	A chat application – the information might include users, messages, and much more.
Variables are used to store this information.
A variable
A variable is a “named storage” for data. We can use variables to store goodies, visitors, and other data.
To create a variable in JavaScript, use the let keyword.
The statement below creates (in other words: declares or defines) a variable with the name “message”:
let message;
Now, we can put some data into it by using the assignment operator =:
let message;

message = 'Hello'; // store the string
The string is now saved into the memory area associated with the variable. We can access it using the variable name:
let message;
message = 'Hello!';

alert(message); // shows the variable content
To be concise, we can combine the variable declaration and assignment into a single line:
let message = 'Hello!'; // define the variable and assign the value

alert(message); // Hello!
We can also declare multiple variables in one line:
let user = 'John', age = 25, message = 'Hello';
That might seem shorter, but we don’t recommend it. For the sake of better readability, please use a single line per variable.
The multiline variant is a bit longer, but easier to read:
let user = 'John';
let age = 25;
let message = 'Hello';
Some people also define multiple variables in this multiline style:
let user = 'John',
  age = 25,
  message = 'Hello';
…Or even in the “comma-first” style:
let user = 'John'
  , age = 25
  , message = 'Hello';
Technically, all these variants do the same thing. So, it’s a matter of personal taste and aesthetics.
var instead of let
In older scripts, you may also find another keyword: var instead of let:
   var message = 'Hello';
The var keyword is almost the same as let. It also declares a variable, but in a slightly different, “old-school” way.
There are subtle differences between let and var, but they do not matter for us yet. We’ll cover them in detail in the chapter The old "var".
A real-life analogy
We can easily grasp the concept of a “variable” if we imagine it as a “box” for data, with a uniquely-named sticker on it.
For instance, the variable message can be imagined as a box labeled "message" with the value "Hello!" in it:
 
We can put any value in the box.
We can also change it as many times as we want:
let message;

message = 'Hello!';

message = 'World!'; // value changed

alert(message);
When the value is changed, the old data is removed from the variable:
 
We can also declare two variables and copy data from one into the other.
let hello = 'Hello world!';

let message;

// copy 'Hello world' from hello into message
message = hello;

// now two variables hold the same data
alert(hello); // Hello world!
alert(message); // Hello world!
Functional languages
It’s interesting to note that functional programming languages, like Scala or Erlang, forbid changing variable values.
In such languages, once the value is stored “in the box”, it’s there forever. If we need to store something else, the language forces us to create a new box (declare a new variable). We can’t reuse the old one.
Though it may seem a little odd at first sight, these languages are quite capable of serious development. More than that, there are areas like parallel computations where this limitation confers certain benefits. Studying such a language (even if you’re not planning to use it soon) is recommended to broaden the mind.
Variable naming
There are two limitations on variable names in JavaScript:
1.	The name must contain only letters, digits, or the symbols $ and _.
2.	The first character must not be a digit.
Examples of valid names:
let userName;
let test123;
When the name contains multiple words, camelCase is commonly used. That is: words go one after another, each word except first starting with a capital letter: myVeryLongName.
What’s interesting – the dollar sign '$' and the underscore '_' can also be used in names. They are regular symbols, just like letters, without any special meaning.
These names are valid:
let $ = 1; // declared a variable with the name "$"
let _ = 2; // and now a variable with the name "_"

alert($ + _); // 3
Examples of incorrect variable names:
let 1a; // cannot start with a digit

let my-name; // hyphens '-' aren't allowed in the name
Case matters
Variables named apple and AppLE are two different variables.
Non-English letters are allowed, but not recommended
It is possible to use any language, including cyrillic letters or even hieroglyphs, like this:
let имя = '...';
let 我 = '...';
Technically, there is no error here, such names are allowed, but there is an international tradition to use English in variable names. Even if we’re writing a small script, it may have a long life ahead. People from other countries may need to read it some time.
Reserved names
There is a list of reserved words, which cannot be used as variable names because they are used by the language itself.
For example: let, class, return, and function are reserved.
The code below gives a syntax error:
let let = 5; // can't name a variable "let", error!
let return = 5; // also can't name it "return", error!
An assignment without use strict
Normally, we need to define a variable before using it. But in the old times, it was technically possible to create a variable by a mere assignment of the value without using let. This still works now if we don’t put use strict in our scripts to maintain compatibility with old scripts.
// note: no "use strict" in this example

num = 5; // the variable "num" is created if it didn't exist

alert(num); // 5
This is a bad practice and would cause an error in strict mode:
"use strict";

num = 5; // error: num is not defined
Constants
To declare a constant (unchanging) variable, use const instead of let:
const myBirthday = '18.04.1982';
Variables declared using const are called “constants”. They cannot be changed. An attempt to do so would cause an error:
const myBirthday = '18.04.1982';

myBirthday = '01.01.2001'; // error, can't reassign the constant!
When a programmer is sure that a variable will never change, they can declare it with const to guarantee and clearly communicate that fact to everyone.
Uppercase constants
There is a widespread practice to use constants as aliases for difficult-to-remember values that are known prior to execution.
Such constants are named using capital letters and underscores.
Like this:
const COLOR_RED = "#F00";
const COLOR_GREEN = "#0F0";
const COLOR_BLUE = "#00F";
const COLOR_ORANGE = "#FF7F00";

// ...when we need to pick a color
let color = COLOR_ORANGE;
alert(color); // #FF7F00
Benefits:
•	COLOR_ORANGE is much easier to remember than "#FF7F00".
•	It is much easier to mistype "#FF7F00" than COLOR_ORANGE.
•	When reading the code, COLOR_ORANGE is much more meaningful than #FF7F00.
When should we use capitals for a constant and when should we name it normally? Let’s make that clear.
Being a “constant” just means that a variable’s value never changes. But there are constants that are known prior to execution (like a hexadecimal value for red) and there are constants that are calculated in run-time, during the execution, but do not change after their initial assignment.
For instance:
const pageLoadTime = /* time taken by a webpage to load */;
The value of pageLoadTime is not known prior to the page load, so it’s named normally. But it’s still a constant because it doesn’t change after assignment.
In other words, capital-named constants are only used as aliases for “hard-coded” values.
Name things right
Talking about variables, there’s one more extremely important thing.
Please name your variables sensibly. Take time to think about this.
Variable naming is one of the most important and complex skills in programming. A quick glance at variable names can reveal which code was written by a beginner versus an experienced developer.
In a real project, most of the time is spent modifying and extending an existing code base rather than writing something completely separate from scratch. When we return to some code after doing something else for a while, it’s much easier to find information that is well-labeled. Or, in other words, when the variables have good names.
Please spend time thinking about the right name for a variable before declaring it. Doing so will repay you handsomely.
Some good-to-follow rules are:
•	Use human-readable names like userName or shoppingCart.
•	Stay away from abbreviations or short names like a, b, c, unless you really know what you’re doing.
•	Make names maximally descriptive and concise. Examples of bad names are data and value. Such names say nothing. It’s only okay to use them if the context of the code makes it exceptionally obvious which data or value the variable is referencing.
•	Agree on terms within your team and in your own mind. If a site visitor is called a “user” then we should name related variables currentUser or newUser instead of currentVisitor or newManInTown.
Sounds simple? Indeed it is, but creating descriptive and concise variable names in practice is not. Go for it.
Reuse or create?
And the last note. There are some lazy programmers who, instead of declaring new variables, tend to reuse existing ones.
As a result, their variables are like boxes into which people throw different things without changing their stickers. What’s inside the box now? Who knows? We need to come closer and check.
Such programmers save a little bit on variable declaration but lose ten times more on debugging.
An extra variable is good, not evil.
Modern JavaScript minifiers and browsers optimize code well enough, so it won’t create performance issues. Using different variables for different values can even help the engine optimize your code.
Summary
We can declare variables to store data by using the var, let, or const keywords.
•	let – is a modern variable declaration. The code must be in strict mode to use let in Chrome (V8).
•	var – is an old-school variable declaration. Normally we don’t use it at all, but we’ll cover subtle differences from let in the chapter The old "var", just in case you need them.
•	const – is like let, but the value of the variable can’t be changed.
Variables should be named in a way that allows us to easily understand what’s inside them.
Tasks
Working with variables
importance: 2
1.	Declare two variables: admin and name.
2.	Assign the value "John" to name.
3.	Copy the value from name to admin.
4.	Show the value of admin using alert (must output “John”).
solution
Giving the right name
importance: 3
1.	Create a variable with the name of our planet. How would you name such a variable?
2.	Create a variable to store the name of a current visitor to a website. How would you name that variable?
solution
Uppercase const?
importance: 4
Examine the following code:
const birthday = '18.04.1982';

const age = someCode(birthday);
Here we have a constant birthday date and the age is calculated from birthday with the help of some code (it is not provided for shortness, and because details don’t matter here).
Would it be right to use upper case for birthday? For age? Or even for both?
const BIRTHDAY = '18.04.1982'; // make uppercase?

const AGE = someCode(BIRTHDAY); // make uppercase?






















8. Data types
A variable in JavaScript can contain any data. A variable can at one moment be a string and at another be a number:
// no error
let message = "hello";
message = 123456;
Programming languages that allow such things are called “dynamically typed”, meaning that there are data types, but variables are not bound to any of them.
There are seven basic data types in JavaScript. Here, we’ll cover them in general and in the next chapters we’ll talk about each of them in detail.
A number
let n = 123;
n = 12.345;
The number type represents both integer and floating point numbers.
There are many operations for numbers, e.g. multiplication *, division /, addition +, subtraction -, and so on.
Besides regular numbers, there are so-called “special numeric values” which also belong to this data type: Infinity, -Infinity and NaN.
•	Infinity represents the mathematical Infinity ∞. It is a special value that’s greater than any number.
We can get it as a result of division by zero:
alert( 1 / 0 ); // Infinity
Or just reference it directly:
alert( Infinity ); // Infinity
•	NaN represents a computational error. It is a result of an incorrect or an undefined mathematical operation, for instance:
alert( "not a number" / 2 ); // NaN, such division is erroneous
NaN is sticky. Any further operation on NaN returns NaN:
alert( "not a number" / 2 + 5 ); // NaN
So, if there’s a NaN somewhere in a mathematical expression, it propagates to the whole result.
Mathematical operations are safe
Doing maths is “safe” in JavaScript. We can do anything: divide by zero, treat non-numeric strings as numbers, etc.
The script will never stop with a fatal error (“die”). At worst, we’ll get NaN as the result.
Special numeric values formally belong to the “number” type. Of course they are not numbers in the common sense of this word.
We’ll see more about working with numbers in the chapter Numbers.
A string
A string in JavaScript must be surrounded by quotes.
let str = "Hello";
let str2 = 'Single quotes are ok too';
let phrase = `can embed ${str}`;
In JavaScript, there are 3 types of quotes.
1.	Double quotes: "Hello".
2.	Single quotes: 'Hello'.
3.	Backticks: `Hello`.
Double and single quotes are “simple” quotes. There’s no difference between them in JavaScript.
Backticks are “extended functionality” quotes. They allow us to embed variables and expressions into a string by wrapping them in ${…}, for example:
                      let name = "John";

// embed a variable
alert( `Hello, ${name}!` ); // Hello, John!

// embed an expression
alert( `the result is ${1 + 2}` ); // the result is 3
The expression inside ${…} is evaluated and the result becomes a part of the string. We can put anything in there: a variable like name or an arithmetical expression like 1 + 2 or something more complex.
Please note that this can only be done in backticks. Other quotes don’t have this embedding functionality!
alert( "the result is ${1 + 2}" ); // the result is ${1 + 2} (double quotes do nothing)
We’ll cover strings more thoroughly in the chapter Strings.
There is no character type.
In some languages, there is a special “character” type for a single character. For example, in the C language and in Java it is char.
In JavaScript, there is no such type. There’s only one type: string. A string may consist of only one character or many of them.
A boolean (logical type)
The boolean type has only two values: true and false.
This type is commonly used to store yes/no values: true means “yes, correct”, and false means “no, incorrect”.
For instance:
let nameFieldChecked = true; // yes, name field is checked
let ageFieldChecked = false; // no, age field is not checked
Boolean values also come as a result of comparisons:
let isGreater = 4 > 1;

alert( isGreater ); // true (the comparison result is "yes")
We’ll cover booleans more deeply in the chapter Logical operators.
The “null” value
The special null value does not belong to any of the types described above.
It forms a separate type of its own which contains only the null value:
let age = null;
In JavaScript, null is not a “reference to a non-existing object” or a “null pointer” like in some other languages.
It’s just a special value which represents “nothing”, “empty” or “value unknown”.
The code above states that age is unknown or empty for some reason.
The “undefined” value
The special value undefined also stands apart. It makes a type of its own, just like null.
The meaning of undefined is “value is not assigned”.
If a variable is declared, but not assigned, then its value is undefined:
let x;

alert(x); // shows "undefined"
Technically, it is possible to assign undefined to any variable:
let x = 123;

x = undefined;

alert(x); // "undefined"
…But we don’t recommend doing that. Normally, we use null to assign an “empty” or “unknown” value to a variable, and we use undefined for checks like seeing if a variable has been assigned.
Objects and Symbols
The object type is special.
All other types are called “primitive” because their values can contain only a single thing (be it a string or a number or whatever). In contrast, objects are used to store collections of data and more complex entities. We’ll deal with them later in the chapter Objects after we learn more about primitives.
The symbol type is used to create unique identifiers for objects. We have to mention it here for completeness, but it’s better to study this type after objects.
The typeof operator
The typeof operator returns the type of the argument. It’s useful when we want to process values of different types differently or just want to do a quick check.
It supports two forms of syntax:
1.	As an operator: typeof x.
2.	As a function: typeof(x).
In other words, it works with parentheses or without them. The result is the same.
The call to typeof x returns a string with the type name:
typeof undefined // "undefined"

typeof 0 // "number"

typeof true // "boolean"

typeof "foo" // "string"

typeof Symbol("id") // "symbol"

typeof Math // "object"  (1)

typeof null // "object"  (2)

typeof alert // "function"  (3)
The last three lines may need additional explanation:
1.	Math is a built-in object that provides mathematical operations. We will learn it in the chapter Numbers. Here, it serves just as an example of an object.
2.	The result of typeof null is "object". That’s wrong. It is an officially recognized error in typeof, kept for compatibility. Of course, null is not an object. It is a special value with a separate type of its own. So, again, this is an error in the language.
3.	The result of typeof alert is "function", because alert is a function of the language. We’ll study functions in the next chapters where we’ll see that there’s no special “function” type in JavaScript. Functions belong to the object type. But typeoftreats them differently. Formally, it’s incorrect, but very convenient in practice.
Summary
There are 7 basic types in JavaScript.
•	number for numbers of any kind: integer or floating-point.
•	string for strings. A string may have one or more characters, there’s no separate single-character type.
•	boolean for true/false.
•	null for unknown values – a standalone type that has a single value null.
•	undefined for unassigned values – a standalone type that has a single value undefined.
•	object for more complex data structures.
•	symbol for unique identifiers.
The typeof operator allows us to see which type is stored in a variable.
•	Two forms: typeof x or typeof(x).
•	Returns a string with the name of the type, like "string".
•	For null returns "object" – this is an error in the language, it’s not actually an object.
In the next chapters, we’ll concentrate on primitive values and once we’re familiar with them, we’ll move on to objects.
Tasks
String quotes
importance: 5
What is the output of the script?
let name = "Ilya";

alert( `hello ${1}` ); // ?

alert( `hello ${"name"}` ); // ?

alert( `hello ${name}` ); // ?









































9.Type Conversions
Most of the time, operators and functions automatically convert the values given to them to the right type.
For example, alert automatically converts any value to a string to show it. Mathematical operations convert values to numbers.
There are also cases when we need to explicitly convert a value to the expected type.
Not talking about objects yet
In this chapter, we won’t cover objects. Instead, we’ll study primitives first. Later, after we learn about objects, we’ll see how object conversion works in the chapter Object to primitive conversion.
ToString
String conversion happens when we need the string form of a value.
For example, alert(value) does it to show the value.
We can also call the String(value) function to convert a value to a string:
let value = true;
alert(typeof value); // boolean

value = String(value); // now value is a string "true"
alert(typeof value); // string
String conversion is mostly obvious. A false becomes "false", null becomes "null", etc.
ToNumber
Numeric conversion happens in mathematical functions and expressions automatically.
For example, when division / is applied to non-numbers:
alert( "6" / "2" ); // 3, strings are converted to numbers
We can use the Number(value) function to explicitly convert a value to a number:
let str = "123";
alert(typeof str); // string

let num = Number(str); // becomes a number 123

alert(typeof num); // number
Explicit conversion is usually required when we read a value from a string-based source like a text form but expect a number to be entered.
If the string is not a valid number, the result of such a conversion is NaN. For instance:
let age = Number("an arbitrary string instead of a number");

alert(age); // NaN, conversion failed
Numeric conversion rules:
Value	Becomes…
undefined	NaN
null	0
true and false	1 and 0
string	Whitespaces from the start and end are removed. If the remaining string is empty, the result is 0. Otherwise, the number is “read” from the string. An error gives NaN.
Examples:
alert( Number("   123   ") ); // 123
alert( Number("123z") );      // NaN (error reading a number at "z")
alert( Number(true) );        // 1
alert( Number(false) );       // 0
Please note that null and undefined behave differently here: null becomes zero while undefined becomes NaN.
Addition ‘+’ concatenates strings
Almost all mathematical operations convert values to numbers. A notable exception is addition +. If one of the added values is a string, the other one is also converted to a string.
Then, it concatenates (joins) them:
alert( 1 + '2' ); // '12' (string to the right)
alert( '1' + 2 ); // '12' (string to the left)
This only happens when at least one of the arguments is a string. Otherwise, values are converted to numbers.
ToBoolean
Boolean conversion is the simplest one.
It happens in logical operations (later we’ll meet condition tests and other similar things) but can also be performed explicitly with a call to Boolean(value).
The conversion rule:
•	Values that are intuitively “empty”, like 0, an empty string, null, undefined, and NaN, become false.
•	Other values become true.
For instance:
alert( Boolean(1) ); // true
alert( Boolean(0) ); // false

alert( Boolean("hello") ); // true
alert( Boolean("") ); // false
Please note: the string with zero "0" is true
Some languages (namely PHP) treat "0" as false. But in JavaScript, a non-empty string is always true.
alert( Boolean("0") ); // true
alert( Boolean(" ") ); // spaces, also true (any non-empty string is true)
Summary
The three most widely used type conversions are to string, to number, and to boolean.
ToString – Occurs when we output something. Can be performed with String(value). The conversion to string is usually obvious for primitive values.
ToNumber – Occurs in math operations. Can be performed with Number(value).
The conversion follows the rules:
Value	Becomes…
undefined	NaN
null	0
true / false	1 / 0
string	The string is read “as is”, whitespaces from both sides are ignored. An empty string becomes 0. An error gives NaN.
ToBoolean – Occurs in logical operations. Can be performed with Boolean(value).
Follows the rules:
Value	Becomes…
0, null, undefined, NaN, ""	false
any other value	true

Most of these rules are easy to understand and memorize. The notable 
exceptions where people usually make mistakes are:
•	undefined is NaN as a number, not 0.
•	"0" and space-only strings like " " are true as a boolean.
Objects aren’t covered here. We’ll return to them later in the chapter 
Object to primitive conversion that is devoted exclusively to objects 
after we learn more basic things about JavaScript.
Tasks
Type conversions
importance: 5
What are results of these expressions?
"" + 1 + 0
"" - 1 + 0
true + false
6 / "3"
"2" * "3"
4 + 5 + "px"
"$" + 4 + 5
"4" - 2
"4px" - 2
7 / 0
"  -9  " + 5
"  -9  " - 5
null + 1
undefined + 1
Think well, write down and then compare with the answer.
