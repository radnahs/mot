10. Operators
We know many operators from school. They are things like addition +, multiplication *, subtraction -, and so on.
In this chapter, we’ll concentrate on aspects of operators that are not covered by school arithmetic.
Terms: “unary”, “binary”, “operand”
Before we move on, let’s grasp some common terminology.
•	An operand – is what operators are applied to. For instance, in the multiplication of 5 * 2 there are two operands: the left operand is 5 and the right operand is 2. Sometimes, people call these “arguments” instead of “operands”.
•	An operator is unary if it has a single operand. For example, the unary negation - reverses the sign of a number:
•	let x = 1;
•	
•	x = -x;
alert( x ); // -1, unary negation was applied
•	An operator is binary if it has two operands. The same minus exists in binary form as well:
•	let x = 1, y = 3;
alert( y - x ); // 2, binary minus subtracts values
Formally, we’re talking about two different operators here: the unary negation (single operand: reverses the sign) and the binary subtraction (two operands: subtracts).
String concatenation, binary +
Now, let’s see special features of JavaScript operators that are beyond school arithmetics.
Usually, the plus operator + sums numbers.
But, if the binary + is applied to strings, it merges (concatenates) them:
let s = "my" + "string";
alert(s); // mystring
Note that if one of the operands is a string, the other one is converted to a string too.
For example:
alert( '1' + 2 ); // "12"
alert( 2 + '1' ); // "21"
See, it doesn’t matter whether the first operand is a string or the second one. The rule is simple: if either operand is a string, the other one is converted into a string as well.
However, note that operations run from left to right. If there are two numbers followed by a string, the numbers will be added before being converted to a string:
alert(2 + 2 + '1' ); // "41" and not "221"
String concatenation and conversion is a special feature of the binary plus +. Other arithmetic operators work only with numbers and always convert their operands to numbers.
For instance, subtraction and division:
alert( 2 - '1' ); // 1
alert( '6' / '2' ); // 3
Numeric conversion, unary +
The plus + exists in two forms: the binary form that we used above and the unary form.
The unary plus or, in other words, the plus operator + applied to a single value, doesn’t do anything to numbers. But if the operand is not a number, the unary plus converts it into a number.
For example:
// No effect on numbers
let x = 1;
alert( +x ); // 1

let y = -2;
alert( +y ); // -2

// Converts non-numbers
alert( +true ); // 1
alert( +"" );   // 0
It actually does the same thing as Number(...), but is shorter.
The need to convert strings to numbers arises very often. For example, if we are getting values from HTML form fields, they are usually strings.
What if we want to sum them?
The binary plus would add them as strings:
let apples = "2";
let oranges = "3";

alert( apples + oranges ); // "23", the binary plus concatenates strings
If we want to treat them as numbers, we need to convert and then sum them:
let apples = "2";
let oranges = "3";

// both values converted to numbers before the binary plus
alert( +apples + +oranges ); // 5

// the longer variant
// alert( Number(apples) + Number(oranges) ); // 5
From a mathematician’s standpoint, the abundance of pluses may seem strange. But from a programmer’s standpoint, there’s nothing special: unary pluses are applied first, they convert strings to numbers, and then the binary plus sums them up.
Why are unary pluses applied to values before the binary ones? As we’re going to see, that’s because of their higher precedence.
Operator precedence
If an expression has more than one operator, the execution order is defined by their precedence, or, in other words, the implicit priority order of operators.
From school, we all know that the multiplication in the expression 1 + 2 * 2 should be calculated before the addition. That’s exactly the precedence thing. The multiplication is said to have a higher precedence than the addition.
Parentheses override any precedence, so if we’re not satisfied with the implicit order, we can use them to change it. For example: (1 + 2) * 2.
There are many operators in JavaScript. Every operator has a corresponding precedence number. The one with the larger number executes first. If the precedence is the same, the execution order is from left to right.
Here’s an extract from the precedence table (you don’t need to remember this, but note that unary operators are higher than corresponding binary ones):
Precedence	Name	Sign
…	…	…
16	unary plus	+
16	unary negation	-
14	Multiplication	*
14	Division	/
13	Addition	+
13	Subtraction	-
…	…	…
3	Assignment	=
…	…	…
As we can see, the “unary plus” has a priority of 16 which is higher than the 13 of “addition” (binary plus). That’s why, in the expression "+apples + +oranges", unary pluses work before the addition.
Assignment
Let’s note that an assignment = is also an operator. It is listed in the precedence table with the very low priority of 3.
That’s why, when we assign a variable, like x = 2 * 2 + 1, the calculations are done first and then the = is evaluated, storing the result in x.
let x = 2 * 2 + 1;

alert( x ); // 5
It is possible to chain assignments:
let a, b, c;

a = b = c = 2 + 2;

alert( a ); // 4
alert( b ); // 4
alert( c ); // 4
Chained assignments evaluate from right to left. First, the rightmost expression 2 + 2 is evaluated and then assigned to the variables on the left: c, b and a. At the end, all the variables share a single value.
The assignment operator "=" returns a value
An operator always returns a value. That’s obvious for most of them like addition + or multiplication *. But the assignment operator follows this rule too.
The call x = value writes the value into x and then returns it.
Here’s a demo that uses an assignment as part of a more complex expression:
let a = 1;
let b = 2;

let c = 3 - (a = b + 1);

alert( a ); // 3
alert( c ); // 0
In the example above, the result of (a = b + 1) is the value which is assigned to a (that is 3). It is then used to subtract from 3.
Funny code, isn’t it? We should understand how it works, because sometimes we see it in 3rd-party libraries, but shouldn’t write anything like that ourselves. Such tricks definitely don’t make code clearer or readable.
Remainder %
The remainder operator %, despite its appearance, is not related to percents.
The result of a % b is the remainder of the integer division of a by b.
For instance:
alert( 5 % 2 ); // 1 is a remainder of 5 divided by 2
alert( 8 % 3 ); // 2 is a remainder of 8 divided by 3
alert( 6 % 3 ); // 0 is a remainder of 6 divided by 3
Exponentiation **
The exponentiation operator ** is a recent addition to the language.
For a natural number b, the result of a ** b is a multiplied by itself b times.
For instance:
alert( 2 ** 2 ); // 4  (2 * 2)
alert( 2 ** 3 ); // 8  (2 * 2 * 2)
alert( 2 ** 4 ); // 16 (2 * 2 * 2 * 2)
The operator works for non-integer numbers as well.
For instance:
alert( 4 ** (1/2) ); // 2 (power of 1/2 is the same as a square root, that's maths)
alert( 8 ** (1/3) ); // 2 (power of 1/3 is the same as a cubic root)
Increment/decrement
Increasing or decreasing a number by one is among the most common numerical operations.
So, there are special operators for it:
•	Increment ++ increases a variable by 1:
•	let counter = 2;
•	counter++;      // works the same as counter = counter + 1, but is shorter
alert( counter ); // 3
•	Decrement -- decreases a variable by 1:
•	let counter = 2;
•	counter--;      // works the same as counter = counter - 1, but is shorter
alert( counter ); // 1
Important:
Increment/decrement can only be applied to variables. Trying to use it on a value like 5++ will give an error.
The operators ++ and -- can be placed either before or after a variable.
•	When the operator goes after the variable, it is in “postfix form”: counter++.
•	The “prefix form” is when the operator goes before the variable: ++counter.
Both of these statements do the same thing: increase counter by 1.
Is there any difference? Yes, but we can only see it if we use the returned value of ++/--.
Let’s clarify. As we know, all operators return a value. Increment/decrement is no exception. The prefix form returns the new value while the postfix form returns the old value (prior to increment/decrement).
To see the difference, here’s an example:
let counter = 1;
let a = ++counter; // (*)

alert(a); // 2
In the line (*), the prefix form ++counter increments counter and returns the new value, 2. So, the alert shows 2.
Now, let’s use the postfix form:
let counter = 1;
let a = counter++; // (*) changed ++counter to counter++

alert(a); // 1
In the line (*), the postfix form counter++ also increments counter but returns the old value (prior to increment). So, the alert shows 1.
To summarize:
•	If the result of increment/decrement is not used, there is no difference in which form to use:
•	let counter = 0;
•	counter++;
•	++counter;
alert( counter ); // 2, the lines above did the same
•	If we’d like to increase a value and immediately use the result of the operator, we need the prefix form:
•	let counter = 0;
alert( ++counter ); // 1
•	If we’d like to increment a value but use its previous value, we need the postfix form:
•	let counter = 0;
alert( counter++ ); // 0
Increment/decrement among other operators
The operators ++/-- can be used inside expressions as well. Their precedence is higher than most other arithmetical operations.
For instance:
let counter = 1;
alert( 2 * ++counter ); // 4
Compare with:
let counter = 1;
alert( 2 * counter++ ); // 2, because counter++ returns the "old" value
Though technically okay, such notation usually makes code less readable. One line does multiple things – not good.
While reading code, a fast “vertical” eye-scan can easily miss something like counter++ and it won’t be obvious that the variable increased.
We advise a style of “one line – one action”:
let counter = 1;
alert( 2 * counter );
counter++;
Bitwise operators
Bitwise operators treat arguments as 32-bit integer numbers and work on the level of their binary representation.
These operators are not JavaScript-specific. They are supported in most programming languages.
The list of operators:
•	AND ( & )
•	OR ( | )
•	XOR ( ^ )
•	NOT ( ~ )
•	LEFT SHIFT ( << )
•	RIGHT SHIFT ( >> )
•	ZERO-FILL RIGHT SHIFT ( >>> )
These operators are used very rarely. To understand them, we need to delve into low-level number representation and it would not be optimal to do that right now, especially since we won’t need them any time soon. If you’re curious, you can read the Bitwise Operators article on MDN. It would be more practical to do that when a real need arises.
Modify-in-place
We often need to apply an operator to a variable and store the new result in that same variable.
For example:
let n = 2;
n = n + 5;
n = n * 2;
This notation can be shortened using the operators += and *=:
let n = 2;
n += 5; // now n = 7 (same as n = n + 5)
n *= 2; // now n = 14 (same as n = n * 2)

alert( n ); // 14
Short “modify-and-assign” operators exist for all arithmetical and bitwise operators: /=, -=, etc.
Such operators have the same precedence as a normal assignment, so they run after most other calculations:
let n = 2;

n *= 3 + 5;

alert( n ); // 16  (right part evaluated first, same as n *= 8)
Comma
The comma operator , is one of the rarest and most unusual operators. Sometimes, it’s used to write shorter code, so we need to know it in order to understand what’s going on.
The comma operator allows us to evaluate several expressions, dividing them with a comma ,. Each of them is evaluated but only the result of the last one is returned.
For example:
let a = (1 + 2, 3 + 4);

alert( a ); // 7 (the result of 3 + 4)
Here, the first expression 1 + 2 is evaluated and its result is thrown away. Then, 3 + 4 is evaluated and returned as the result.
Comma has a very low precedence
Please note that the comma operator has very low precedence, lower than =, so parentheses are important in the example above.
Without them: a = 1 + 2, 3 + 4 evaluates + first, summing the numbers into a = 3, 7, then the assignment operator = assigns a = 3, and finally the number after the comma, 7, is not processed so it’s ignored.
Why do we need an operator that throws away everything except the last part?
Sometimes, people use it in more complex constructs to put several actions in one line.
For example:
 // three operations in one line
for (a = 1, b = 3, c = a * b; a < 10; a++) {
 ...
}
Such tricks are used in many JavaScript frameworks. That’s why we’re mentioning them. But, usually, they don’t improve code readability so we should think well before using them.
Tasks
The postfix and prefix forms
importance: 5
What are the final values of all variables a, b, c and d after the code below?
let a = 1, b = 1;

let c = ++a; // ?
let d = b++; // ?
solution
Assignment result
importance: 3
What are the values of a and x after the code below?
let a = 2;

let x = 1 + (a *= 2);





11. Comparisons
We know many comparison operators from maths:
•	Greater/less than: a > b, a < b.
•	Greater/less than or equals: a >= b, a <= b.
•	Equals: a == b (please note the double equals sign =. A single symbol a = b would mean an assignment).
•	Not equals. In maths the notation is ≠, but in JavaScript it’s written as an assignment with an exclamation sign before it: a != b.
Boolean is the result
Like all other operators, a comparison returns a value. In this case, the value is a boolean.
•	true – means “yes”, “correct” or “the truth”.
•	false – means “no”, “wrong” or “not the truth”.
For example:
alert( 2 > 1 );  // true (correct)
alert( 2 == 1 ); // false (wrong)
alert( 2 != 1 ); // true (correct)
A comparison result can be assigned to a variable, just like any value:
let result = 5 > 4; // assign the result of the comparison
alert( result ); // true
String comparison
To see whether a string is greater than another, JavaScript uses the so-called “dictionary” or “lexicographical” order.
In other words, strings are compared letter-by-letter.
For example:
alert( 'Z' > 'A' ); // true
alert( 'Glow' > 'Glee' ); // true
alert( 'Bee' > 'Be' ); // true
The algorithm to compare two strings is simple:
1.	Compare the first character of both strings.
2.	If the first character from the first string is greater (or less) than the other string’s, then the first string is greater (or less) than the second. We’re done.
3.	Otherwise, if both strings’ first characters are the same, compare the second characters the same way.
4.	Repeat until the end of either string.
5.	If both strings end at the same length, then they are equal. Otherwise, the longer string is greater.
In the examples above, the comparison 'Z' > 'A' gets to a result at the first step while the strings "Glow" and "Glee" are compared character-by-character:
1.	G is the same as G.
2.	l is the same as l.
3.	o is greater than e. Stop here. The first string is greater.
Not a real dictionary, but Unicode order
The comparison algorithm given above is roughly equivalent to the one used in dictionaries or phone books, but it’s not exactly the same.
For instance, case matters. A capital letter "A" is not equal to the lowercase "a". Which one is greater? The lowercase "a". Why? Because the lowercase character has a greater index in the internal encoding table JavaScript uses (Unicode). We’ll get back to specific details and consequences of this in the chapter Strings.
Comparison of different types
When comparing values of different types, JavaScript converts the values to numbers.
For example:
alert( '2' > 1 ); // true, string '2' becomes a number 2
alert( '01' == 1 ); // true, string '01' becomes a number 1
For boolean values, true becomes 1 and false becomes 0.
For example:
alert( true == 1 ); // true
alert( false == 0 ); // true
A funny consequence
It is possible that at the same time:
•	Two values are equal.
•	One of them is true as a boolean and the other one is false as a boolean.
For example:
let a = 0;
alert( Boolean(a) ); // false

let b = "0";
alert( Boolean(b) ); // true

alert(a == b); // true!
From JavaScript’s standpoint, this result is quite normal. An equality check converts values using the numeric conversion (hence "0" becomes 0), while the explicit Boolean conversion uses another set of rules.
Strict equality
A regular equality check == has a problem. It cannot differentiate 0 from false:
alert( 0 == false ); // true
The same thing happens with an empty string:
alert( '' == false ); // true
This happens because operands of different types are converted to numbers by the equality operator ==. An empty string, just like false, becomes a zero.
What to do if we’d like to differentiate 0 from false?
A strict equality operator === checks the equality without type conversion.
In other words, if a and b are of different types, then a === b immediately returns false without an attempt to convert them.
Let’s try it:
alert( 0 === false ); // false, because the types are different
There is also a “strict non-equality” operator !== analogous to !=.
The strict equality operator is a bit longer to write, but makes it obvious what’s going on and leaves less room for errors.
Comparison with null and undefined
Let’s see more edge cases.
There’s a non-intuitive behavior when null or undefined are compared to other values.
For a strict equality check ===
These values are different, because each of them is a different type.
alert( null === undefined ); // false
For a non-strict check ==
There’s a special rule. These two are a “sweet couple”: they equal each other (in the sense of ==), but not any other value.
alert( null == undefined ); // true
For maths and other comparisons < > <= >=
null/undefined are converted to numbers: null becomes 0, while undefined becomes NaN.
Now let’s see some funny things that happen when we apply these rules. And, what’s more important, how to not fall into a trap with them.
Strange result: null vs 0
Let’s compare null with a zero:
alert( null > 0 );  // (1) false
alert( null == 0 ); // (2) false
alert( null >= 0 ); // (3) true
Mathematically, that’s strange. The last result states that "null is greater than or equal to zero", so in one of the comparisons above it must be true, but they are both false.
The reason is that an equality check == and comparisons > < >= <= work differently. Comparisons convert null to a number, treating it as 0. That’s why (3) null >= 0 is true and (1) null > 0 is false.
On the other hand, the equality check == for undefined and null is defined such that, without any conversions, they equal each other and don’t equal anything else. That’s why (2) null == 0 is false.
An incomparable undefined
The value undefined shouldn’t be compared to other values:
alert( undefined > 0 ); // false (1)
alert( undefined < 0 ); // false (2)
alert( undefined == 0 ); // false (3)
Why does it dislike zero so much? Always false!
We get these results because:
•	Comparisons (1) and (2) return false because undefined gets converted to NaN and NaN is a special numeric value which returns false for all comparisons.
•	The equality check (3) returns false because undefined only equals null and no other value.
Evade problems
Why did we go over these examples? Should we remember these peculiarities all the time? Well, not really. Actually, these tricky things will gradually become familiar over time, but there’s a solid way to evade problems with them:
Just treat any comparison with undefined/null except the strict equality === with exceptional care.
Don’t use comparisons >= > < <= with a variable which may be null/undefined, unless you’re really sure of what you’re doing. If a variable can have these values, check for them separately.
Summary
•	Comparison operators return a boolean value.
•	Strings are compared letter-by-letter in the “dictionary” order.
•	When values of different types are compared, they get converted to numbers (with the exclusion of a strict equality check).
•	The values null and undefined equal == each other and do not equal any other value.
•	Be careful when using comparisons like > or < with variables that can occasionally be null/undefined. Checking for null/undefined separately is a good idea.
Tasks
Comparisons
importance: 5
What will be the result for these expressions?
5 > 4
"apple" > "pineapple"
"2" > "12"
undefined == null
undefined === null
null == "\n0\n"
null === +"\n0\n"















12. Interaction: alert, prompt, confirm
This part of the tutorial aims to cover JavaScript “as is”, without environment-specific tweaks.
But we’ll still be using the browser as our demo environment, so we should know at least a few of its user-interface functions. In this chapter, we’ll get familiar with the browser functions alert, prompt and confirm.
alert
Syntax:
alert(message);
This shows a message and pauses script execution until the user presses “OK”.
For example:
alert("Hello");
The mini-window with the message is called a modal window. The word “modal” means that the visitor can’t interact with the rest of the page, press other buttons, etc. until they have dealt with the window. In this case – until they press “OK”.
prompt
The function prompt accepts two arguments:
result = prompt(title, [default]);
It shows a modal window with a text message, an input field for the visitor, and the buttons OK/CANCEL.
title
The text to show the visitor.
default
An optional second parameter, the initial value for the input field.
The visitor may type something in the prompt input field and press OK. Or they can cancel the input by pressing CANCEL or hitting the Esc key.
The call to prompt returns the text from the input field or null if the input was canceled.
For instance:
let age = prompt('How old are you?', 100);

alert(`You are ${age} years old!`); // You are 100 years old!
In IE: always supply a default
The second parameter is optional, but if we don’t supply it, Internet Explorer will insert the text "undefined" into the prompt.
Run this code in Internet Explorer to see:
let test = prompt("Test");
So, for prompts to look good in IE, we recommend always providing the second argument:
let test = prompt("Test", ''); // <-- for IE
confirm
The syntax:
result = confirm(question);
The function confirm shows a modal window with a question and two buttons: OK and CANCEL.
The result is true if OK is pressed and false otherwise.
For example:
let isBoss = confirm("Are you the boss?");

alert( isBoss ); // true if OK is pressed
Summary
We covered 3 browser-specific functions to interact with visitors:
alert
shows a message.
prompt
shows a message asking the user to input text. It returns the text or, if CANCEL or Esc is clicked, null.
confirm
shows a message and waits for the user to press “OK” or “CANCEL”. It returns true for OK and false for CANCEL/Esc.
All these methods are modal: they pause script execution and don’t allow the visitor to interact with the rest of the page until the window has been dismissed.
There are two limitations shared by all the methods above:
1.	The exact location of the modal window is determined by the browser. Usually, it’s in the center.
2.	The exact look of the window also depends on the browser. We can’t modify it.
That is the price for simplicity. There are other ways to show nicer windows and richer interaction with the visitor, but if “bells and whistles” do not matter much, these methods work just fine.
Tasks
A simple page
importance: 4
Create a web-page that asks for a name and outputs it.
